<?php
session_start();

header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('America/Sao_Paulo');

/*
 * Autoload para o Composer
 */
$autoload = dirname(__FILE__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

if(file_exists($autoload)) require $autoload;
require dirname(__FILE__).DIRECTORY_SEPARATOR.'configuracoes'.DIRECTORY_SEPARATOR.'configuracoes.php';
require dirname(__FILE__).DS.'configuracoes'.DS.'funcoes.php';
require dirname(__FILE__).DS.'configuracoes'.DS.'seo.php';
