<?php
require dirname(__FILE__).DIRECTORY_SEPARATOR.'init.php';

if(isset($_POST['ajax'])) {

	// Entrar em contato
	if($_POST['ajax'] == 'entrarEmContato') {
		validarCampos($_POST, array('Nome', 'Telefone', 'Mensagem'));

		$nomeRemetente = isset($_POST['Nome']) ? $_POST['Nome'] : $config['formulario']['nomeRemetente'];
		$emailRemetente = isset($_POST['Email']) ? $_POST['Email'] : $config['formulario']['emailRemetente'];

		$enviarEmail = enviarEmail($_POST, $nomeRemetente, $emailRemetente, 'Contato através do site - '.$config['site']['nomeSite']);

		exit(json_encode($enviarEmail));
	}

	// Orçamento
	if($_POST['ajax'] == 'solicitarOrcamento') {
		validarCampos($_POST, array('Nome', 'Telefone', 'Email', 'Mensagem'));

		$nomeRemetente = isset($_POST['Nome']) ? $_POST['Nome'] : $config['formulario']['nomeRemetente'];
		$emailRemetente = isset($_POST['Email']) ? $_POST['Email'] : $config['formulario']['emailRemetente'];

		$enviarEmail = enviarEmail($_POST, $nomeRemetente, $emailRemetente, 'Orçamento através do site - '.$config['site']['nomeSite'], $_FILES);

		exit(json_encode($enviarEmail));
	}
}
