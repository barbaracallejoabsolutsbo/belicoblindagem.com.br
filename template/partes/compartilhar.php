<div class="redes-sociais compartilhar">
	<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo URL.ltrim(PAGINA, '/'); ?>&t=<?php echo urlencode(TITULO); ?>" target="_blank" class="cor-facebook">
		<i class="fa fa-facebook"></i>
	</a>

	<a href="http://www.twitter.com/intent/tweet?url=<?php echo URL.ltrim(PAGINA, '/'); ?>&text=<?php echo urlencode(TITULO); ?>" target="_blank" class="cor-twitter">
		<i class="fa fa-twitter"></i>
	</a>

	<a href="http://plus.google.com/share?url=<?php echo URL.ltrim(PAGINA, '/'); ?>" target="_blank" class="cor-google-plus">
		<i class="fa fa-google-plus"></i>
	</a>
</div>