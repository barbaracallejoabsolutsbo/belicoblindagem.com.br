<?php
echo seo(PAGINA);

echo cssInline(array(
	'style.css' => true,
	'responsivo.css' => true,
	'font-awesome.min.css' => true,
	'magnific-popup.css' => true,
	'slick-theme.css' => true,
	'slick.css' => true,
	'bootstrap.css' => true,	
	'nivo-slider.css' => true,
	'informacoes.css' => true,
	'hover.css' => true,	
	'set1.css' => true,	
));

echo '<script>const URL = "'.URL.'"</script>';
