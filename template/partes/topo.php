<div class="faixa-cinza sticky">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p><i class="fa fa-phone" aria-hidden="true"></i> (11) 3088-8383 / <i class="fa fa-whatsapp" aria-hidden="true"></i> <a href="https://wa.me/5511987423131?text=Ola%20tudo%20bem?" target="_blank">(11) 98742-3131</a>
				</p>
			</div>
			<div class="col-md-6 text-right">
				<div class="icon-faixa">

					“Trabalhamos com Qualidade e Prosperamos com Alegria e Amizade”

				</div>
			</div>
		</div>
	</div>
</div>
<header class="topo">
	<div class="container">
		<div class="topo-box">
			<div class="logo">
				<a href="<?php echo URL; ?>"><img src="<?php echo $config['urls']['imagens']; ?>logo.png" alt="logo" /></a>
			</div>
			<nav class="menu" data-menu-principal>
				<div data-btn-menu-responsivo class="btn-menu-responsivo">
					<div class="btn-menu" data-menu>MENU</div>
				</div>
				<div class="menu-box">
					<ul>
						<li><a href="<?php echo URL; ?>" title="Home" class="hvr-grow">Início</a></li>
						<li><a href="<?php echo URL; ?>blindagem" title="Blindagem" class="hvr-grow">Blindagem</a></li>
						<li><a href="<?php echo URL; ?>servicos" title="Serviços" class="hvr-grow">Serviços</a></li>
						<li><a href="<?php echo URL; ?>empresa" title="Empresa" class="hvr-grow">Empresa</a></li>

						<li><a href="<?php echo URL; ?>parcerias" title="Parceria" class="hvr-grow">Parcerias</a></li>
						<li>
							<a href="<?php echo URL; ?>informacoes" title="Informações" class="hvr-icon-hang">Informações <i class="fa fa-chevron-down hvr-icon"></i></a>
							<ul>
								<?php foreach ($seo['paginasSeo'] as $key => $value) {
									echo '<li><a href="'.rtrim(URL, '/').$key.'">'.$value['title'].'</a></li>';
								} ?>
							</ul>
						</li>
						<li><a href="<?php echo URL; ?>contato" title="Contato" class="hvr-grow">Contato</a></li>
						<li>
							<p>
								<a href="<?php echo URL; ?>orcamento" title="Orçamento">Orçamento</a>
							</p>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>