<div class="texto-direitos-autorais">
	O texto acima "<?php echo TITULO; ?>" é de direito reservado. Sua reprodução, parcial ou total, mesmo citando nossos links, é proibida sem a autorização do autor. Plágio é crime e está previsto no artigo 184 do Código Penal. – <a href="http://www.planalto.gov.br/Ccivil_03/Leis/L9610.htm" target="_blank">Lei n° 9.610-98 sobre os Direitos Autorais</a>.
</div>