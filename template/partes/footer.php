<footer class="footer">
	<div class="container">	
		<div class="creditos">
			<div class="row">
				<div class="col-md-4 text-right">
					<p><i class="fa fa-phone" aria-hidden="true"></i> (11) 3088-8383 / <br><i class="fa fa-envelope-o" aria-hidden="true"></i> belicoblindagem@belicoblindagem.com.br</p>
				</div>
				<div class="col-md-4 text-center">
					<div class="reforco">
						<a href="<?php echo URL; ?>informacoes" title="Informações">Informaçõoes</a>
						<a href="<?php echo URL; ?>mapa-site" title="Mapa do Site">Mapa do Site</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="creditos-link">
						<a href="<?php echo $config['creditos']['link']; ?>" target="_blank"><img src="<?php echo $config['urls']['imagens']; ?>creditos/logo.png" alt="logo"></a>
						<a href="http://validator.w3.org/check?uri=<?php echo rtrim(URL, '/').PAGINA; ?>" target="_blank"><img src="<?php echo $config['urls']['imagens']; ?>creditos/logo-html5.png" alt="html5" style="width: 20px;"></a>
						<a href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo rtrim(URL, '/').PAGINA; ?>" target="_blank"><img src="<?php echo $config['urls']['imagens']; ?>creditos/logo-css3.png" alt="css3" style="width: 20px;"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="menu-lateral" data-menu-lateral>
	<div class="menu-lateral-conteudo" data-menu-lateral-conteudo></div>
	<div data-menu class="menu-lateral-overlay"></div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154246345-12"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-154246345-12');
</script>

<div class="barra-footer">
	<a href="tel:113088-8383"><i class="fa fa-phone"></i></a>	
	<a href="https://wa.me/5511987423131?text=Ola%20tudo%20bem?"><i class="fa fa-whatsapp"></i></a>	
	<a href="mailto:belicoblindagem@belicoblindagem.com.br"><i class="fa fa-envelope"></i></a>
</div>

<!-- Botão de indução -->
<!-- <div class="botao-fixo">
	<div class="whatsapp">
		<p> <a href="https://wa.me/5511987423131?text=Ola%20tudo%20bem?" alt="whatsapp" target="_blank"> <i class="fa fa-whatsapp"></i> (11) 98742-0954</a> </p>
	</div>
	<div class="telefone">
		<p> <a href="tel:1130888383" alt="telefone" target="_blank"> <i class="fa fa-phone" aria-hidden="true"></i> (11) 3088-8383</a> </p>
	</div>
	<div class="email">
		<p> <a href="mailto:belicoblindagem@belicoblindagem.com.br" alt="email" target="_blank"> <i class="fa fa-envelope-o" aria-hidden="true"></i> belicoblindagem@belicoblindagem.com.br</a> </p>
	</div>
</div> -->


<?php
echo jsInline(array(
	'jquery-3.3.1.min.js' => false,
	'slick.min.js' => false,
	'jquery.magnific-popup.min.js' => false,
	'functions.js' => true,
	'scripts.js' => true,
	'jquery.nivo.js' => true,
	'jquery.slick.js' => true,	
));


