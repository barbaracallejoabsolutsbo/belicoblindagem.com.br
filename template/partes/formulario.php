<div class="formulario-container">
	<form>
		<div class="formulario-box">
			<label>Nome</label>
			<div>
				<input type="text" name="Nome" />
			</div>
		</div>
		<div class="formulario-box">
			<label>Telefone</label>
			<div>
				<input type="text" name="Telefone" />
			</div>
		</div>
		<div class="formulario-box">
			<label>E-mail</label>
			<div>
				<input type="email" name="Email" />
			</div>
		</div>
		<div class="formulario-box">
			<div>
				<select name="Como nos Conheceu">
					<option value="Como nos conheceu?">Como nos conheceu?</option>
					<option value="Google">Google</option>
					<option value="Facebook">Facebook</option>
					<option value="Marketing">Marketing</option>
					<option value="Outdoor">Outdoor</option>
					<option value="Folders">Folders</option>
					<option value="Rádio">Rádio</option>
					<option value="Websites">Websites</option>
					<option value="Outros Canais">Outros Canais</option>
				</select>
			</div>
		</div>			
		<div class="formulario-box">
			<label>Mensagem</label>
			<div>
				<textarea name="Mensagem"></textarea>
			</div>
		</div>
		<div class="formulario-box align-center">
			<button type="submit" class="btn btn-default" data-ajax="entrarEmContato">Enviar Mensagem</button>
		</div>
	</form>
</div>