<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Parcerias</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="parceiros">
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center">
							<div class="col-md-4 links-marcas">
								<img src="<?php echo $config['urls']['imagens']; ?>akta.jpg" alt="Empresa Full" class="img-responsive"/>
								<a href="www.aktamotors.com.br">www.aktamotors.com.br</a>
								<br>
							</div>
							<div class="col-md-4 links-marcas">
								<img src="<?php echo $config['urls']['imagens']; ?>kiavig.jpg" alt="Empresa Full" class="img-responsive"/>
								<a href="www.kiavig.com.br">www.kiavig.com.br</a>
								<br>
							</div>
							<div class="col-md-4 links-marcas">
								<img src="<?php echo $config['urls']['imagens']; ?>kinmotor.jpg" alt="Empresa Full" class="img-responsive"/>
								<a href="www.nissankin.com.br">www.nissankin.com.br</a>
								<br>
							</div>
							<div class="col-md-4 links-marcas">
								<img src="<?php echo $config['urls']['imagens']; ?>vigo.jpg" alt="Empresa Full" class="img-responsive"/>
								<a href="www.vigomotors.com.br">www.vigomotors.com.br</a>
								<br>
							</div>
							<div class="col-md-4 links-marcas">
								<img src="<?php echo $config['urls']['imagens']; ?>vigorito.jpg" alt="Empresa Full" class="img-responsive"/>
								<a href="www.vigorito.com.br">www.vigorito.com.br</a>
								<br>
							</div>
						</div>
						<!-- <div class="col-md-4 face">
							<div id="fb-root"></div>
							<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v6.0"></script>
							<div class="fb-page" data-href="https://www.facebook.com/blindagembelico/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/blindagembelico/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/blindagembelico/">Bélico Blindagem</a></blockquote></div>

						</div> -->
					</div>
				</div>	
			</div>			
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>