<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Parceiros</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="parceiros">
				<div class="container">
					<img src="<?php echo $config['urls']['imagens']; ?>logo-cpi.jpg" alt="Empresa Full" class="img-responsive"/>
					<h4>A Braslimp Serviços e Ceiling Pro International, parceria de sucesso.</h4>
					<p>A empresa se tornou parceira da empresa Ceiling Pro international, no qual, é especializada em limpezas a seco em todo o mundo, (limpeza ecológica e sustentável) Fundada há mais de 26 anos atrás, a Ceiling Pro International (CPI) é a líder entre as empresas de limpeza ecológica de fachadas, paredes, teto, forros, divisórias, com rede nacional e internacional inclui mais de 500 concessionários CPI em 47 países, que prestam serviços a milhares de clientes satisfeitos todos os dias. A CPI está sediada em Minnesota – USA.</p>
					<p>Atuamos como concessionário local da Empresa Ceiling Pro International com produtos ecologicamente corretos, ou seja, produtos de qualidade, que não agridem o meio ambiente, certificados e homologados no Brasil.</p>
					<p>Mediante ao compromisso sério com o meio ambiente e com os recursos naturais, através dos produtos biodegradáveis que utilizamos A <strong>Braslimp</strong> vem gradativamente conquistando seu espaço com grande credibilidade junto a condomínios, bancos, industrias, hipermercados, shopping centers, indústrias farmacêuticas, e em diversos segmentos do estado de São Paulo. Nossos profissionais são qualificados, treinados e capacitados em departamento técnico, e estão aptos a desenvolver um trabalho personalizado, organizado, coerente com suas ideias e anseios.</p>
					<p>Estamos à inteira disposição, esclarecendo quaisquer dúvidas, para que num futuro próximo, possamos concretizar uma grande parceria.</p>
					<p>A estratégia da <strong>Braslimp</strong> foi de inovar, ser pioneira numa prestação de serviço diferenciada da existente no mercado. Ao tomarmos conhecimento deste método que oferece soluções de limpeza sem o uso da água, enxergamos também a oportunidade de dar a nossa parcela de contribuição ao meio ambiente e ao futuro do planeta de forma sustentável para as próximas gerações.</p>
					<p>A utilização racional da água, com a menor geração possível de efluentes e resíduos, reduzindo o impacto ao meio ambiente, são práticas de responsabilidade ambiental, preocupação típica de quem pensa em sustentabilidade.</p>
					<p>Ao iniciar um novo negócio com métodos diferenciados, com plenas e reais condições de atender aos mais diversificados segmentos do mercado. Preocupação em contribuir para um meio ambiente carente de ações independentes, em prol do desenvolvimento sustentável das futuras gerações, é característica de uma empresa responsável com o planeta.</p>
				</div>
			</div>	
		</div>			
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>