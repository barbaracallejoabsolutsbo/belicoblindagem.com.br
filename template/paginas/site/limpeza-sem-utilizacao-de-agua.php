<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Limpeza sem utilização de água</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="obras-full text-center">
				<div class="container">
					<div class="img-pc">
						<a href="<?php echo URL; ?>template/imagens/obras/limpeza-sem-agua.jpg" data-lightbox="">
							<img src="<?php echo URL; ?>template/imagens/obras/limpeza-sem-agua.jpg" alt="" class="img-responsive">
						</a>
					</div>
					<h4>Project Description:</h4>
					<p>Nossa empresa Trabalha com Limpeza de Fachadas em Pele de Vidros, Alucobond, Granitos Polidos, Superfícies Lisas e Polidas.</p>
				</div>	
			</div>	
		</div>			
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>