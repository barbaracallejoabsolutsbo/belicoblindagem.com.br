<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Blindagem nível III-A.</h2>
							<p style="color: #fff">"Aqui nossos profissionais fazem o que a máquina não faz, o acabamento PERFEITO!"</p>

						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="quems-somos-full">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<br>
							<h3>PROCESSO DE BLINDAGEM BÉLICO</h3>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo01.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Check-list:</h4>
									<p>Início do cadastro do cliente e preparação da Ordem de Serviço adequada para cada veículo e instruções para blindagem.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo02.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Desmontagem::</h4>
									<p>Revestimento de todas as peças abaixo com plástico de proteção: bancos, forrações de portas, máquinas de vidro, pestanas, auto falantes, tampão traseiro, forro do teto, colunas A. B e C, chicote, churrasqueira, pedaleiras, cintos de segurança, retrovisores e fechaduras.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo03.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Instalação do painel balístico (manta de aramida):</h4>
									<p>Portas blindadas com painéis de manta, mínimo 9 (nove) camadas, fixadas com cola de alta performance. As áreas a serem cobertas consistem em: teto, meio para-lama direito e esquerdo, painel inferior ao para-brisa, painel traseiro (encosto de banco e tampão traseiro) e caixas de roda traseira.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo04.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Instalação do aço inox 304L:</h4>
									<p>2,5mm revestido, colado e parafusado.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo05.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Instalação dos vidros blindados:</h4>
									<p>Os vidros originais são substituídos por vidros balísticos possuindo de 17mm até 21mm de espessura. Os vidros são fixados com colas de alta performance. Os vidros dianteiros são fixados com suportes especiais e reforços nas máquinas de vidros além de amortecedores, possibilitando a abertura de 80% dos vidros dianteiros. Os vidros traseiros são fixos.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo06.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Montagem interna:</h4>
									<p>A preparação da montagem consiste na limpeza do veículo (aspiração), instalação de airbags, cinto de segurança, borrachas das portas, preparação de forração de teto, coluna A, B e C, revestimento do porta-malas, quebra sol e alça de segurança, retrovisor interno e bancos.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo07.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Tapeçaria:</h4>
									<p>Instalação das forrações de portas, alto-falantes e plástico de proteção.</p>
								</div>
							</div>
							<hr>
							<h2>ACESSÓRIOS INCLUIDOS:</h2>
							<hr>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo08.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Reforço de suspensão::</h4>
									<p>Serão substituídas as molas traseiras originais por molas reforçadas.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo099.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Instalação de sirene:</h4>
									<p>Sirene de 3 tons e comunicador interno.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo09.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Cinta de aço:</h4>
									<p>Inox 430 de 1.5 mm com certificado de garantia, lacre de segurança e máxima qualidade. Pode ser facilmente adaptada a qualquer modelo de roda. Caso os pneus também sejam perfurados, as cintas de aço manterão o pneu no lugar, possibilitando assim a dirigibilidade do carro e o escape seguro da zona de perigo por até 20km.</p>
								</div>
							</div>
							<hr>
							<div class="row espacamento">
								<div class="col-md-2">
									<img src="<?php echo $config['urls']['imagens']; ?>processo10.png" alt="Empresa Full" class="img-responsive"/>
								</div>
								<div class="col-md-9">
									<h4>Teste do veículo:</h4>
									<p>Verificação de ruídos, infiltração de água, movimentação dos vidros, regulagem das portas, acabamentos, limpeza, polimento e cristalização.</p>
								</div>
							</div>				
						</div>
						<div class="col-md-4">
							<h3>Fotos</h3>
							<div class="theme-default-nivo-slider">
								<div id="slider" class="nivoSlider"> 
									<a href="<?php echo URL; ?>template/imagens/empresa/thumb/01.jpg" data-lightbox="">
										<img src="<?php echo $config['urls']['imagens']; ?>/empresa/01.jpg" />
									</a>
									<a href="<?php echo URL; ?>template/imagens/empresa/thumb/02.jpg" data-lightbox="">
										<img src="<?php echo $config['urls']['imagens']; ?>/empresa/02.jpg" />
									</a>
									<a href="<?php echo URL; ?>template/imagens/empresa/thumb/03.jpg" data-lightbox="">
										<img src="<?php echo $config['urls']['imagens']; ?>/empresa/03.jpg" />
									</a>
									<a href="<?php echo URL; ?>template/imagens/empresa/thumb/04.jpg" data-lightbox="">
										<img src="<?php echo $config['urls']['imagens']; ?>/empresa/04.jpg" />
									</a>
								</div>
							</div>
							<h3>Manta de aramida</h3>
							<p>É um dos principais produtos usados na blindagem porque pode reduzir em até 130kg o resultado final do trabalho. O aço, que anteriormente revestia todo o carro, fica agora apenas nas molduras das portas, espelhos retrovisores, fechaduras, maçanetas e nas pontas dos vidros.</p>
							<p>Além da “leveza”, a flexibilidade se apresenta como uma outra excelente característica da manta de aramida porque possibilita que ela se acomode em diferentes partes do veículo como se fosse uma roupa de encaixe perfeito.</p>
							<hr>
							<h3>Vidro de 17MM nível IIIA</h3>
							<p>Tecnologia máxima em blindagem. Suporta até disparos de Magnum 44 e pesa 27% menos. O VIDRO DE (17 mm* de espessura) é o que há de mais avançado em vidro blindado automotivo. Graças à tecnologia de laminação com duplo policarbonato de alta resistência, oferece o mesmo nível de proteção dos vidros 21 mm, com a vantagem de ser 27% mais leve, o que significa melhor dirigibilidade, menor consumo de combustível e o mesmo nível de proteção até IIIA (o padrão mais utilizado para carros de passeio). É por este conjunto de performances que tem a melhor relação custo-benefício do mercado.</p>
							<p>Indicado para carros de passeio nacionais e importados, é certificado pelo Exército Brasileiro - RETEX, além de atender também a norma NIJ 0108.01 (USA) e NBR 15000.</p>
							<p>Outro ponto fundamental é a proteção das bordas dos vidros (partes mais finas e que ficam embutidas) com pefís de aço** que protege áreas vulneráveis.</p>
							<p>*A espessura mencionada é uma referência nominal, podendo haver variações.</p>
							<h3>Vidros de 21MM nível IIIA</h3>
							<p>(Convensional)</p>
							<p>O vidro blindado de 21 mm*, o padrão mais utilizado pelo mercado na blindagem de carros de passeio. Trata-se de uma solução tradicional, com policarbonato de alta resistência, que oferece proteção até nível IIIA (suporta até disparos de Magnum 44).</p>
							<p>Indicado para carros de passeio nacionais e importados, é certificado pelo Exército Brasileiro - RETEX, além de atender também a norma NIJ 0108.01 (USA) e NBR 15000.</p>
							<p>Outro ponto fundamental é a proteção das bordas dos vidros (partes mais finas e que ficam embutidas) com pefís de aço** que protege áreas vulneráveis.</p>
							<p>*A espessura mencionada é uma referência nominal, podendo haver variações.</p>
							<hr>
						</div>	
					</div>
						<h2>TRABALHAMOS COM AS MELHORES FÁBRICAS DE VIDROS!</h2>

				</div>
			</div>
		</div>	
	</div>				
</main>

<?php require PARTE.'footer.php'; ?>
<script>
	$(function(){
		$("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });
    </script>

</body>
</html>