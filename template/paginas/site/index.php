<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="theme-default-nivo-slider">
				<div id="slider" class="nivoSlider"> 
					<img src="<?php echo $config['urls']['imagens']; ?>slide-1.png" alt="Banner" />
					<img src="<?php echo $config['urls']['imagens']; ?>slide-2.png" alt="Banner" />
				</div>
			</div>	
			<div class="obras-home">
				<div class="container">
					<h2><span>|</span> Quem Somos</h2>
					<p>A BÉLICO BLINDAGEM foi criada para proporcionar a você cliente especial a oportunidade de proteger seu bem mais precioso, sua família! Utilizamos materiais conceituados no mercado, mão de obra altamente qualificada, com atenção minuciosa aos detalhes de acabamento, apresentando assim, um serviço sob medida para cada cliente. Pois aqui você é ÚNICO!</p>
					<br>
					<a href="<?php echo URL; ?>#" title="" class="hvr-grow">Veja Mais</a>
					<div class="borda"></div>
				</div>
			</div>	
			<div class="servicos">
				<div class="container">
					<h4><span>|</span> Depoimentos</h4>
					<div class="item">
						<div>
							<div class="row">								
								<div class="col-md-12">
									<h2>SR. JOHN ANTHONY VON CHRISTIAN</h2>
									<p>"Já blindei 2 carros na Bélico Blindagem, ambos ficaram impecáveis, agradeço ao profissionalismo no atendimento."</p>
								</div>

							</div>
						</div> 
						<div>
							<div class="row">
								<div class="col-md-12">
									<h2>GUSTAVO GOTFRYD, DIRETOR DO GRUPO KIA AKTA MOTORS</h2>
									<p>"Temos uma parceria de 2 anos com a Bélico Blindagem, e minha concessionaria já blindou mais de 100 veículos, já trabalhamos com diversas blindadoras, e com certeza esta é a melhor!"</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-apresentacao">
				<div class="container">
					<div class="obras-home">
						<h2><span>|</span> Veja o que fazemos:</h2>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="box-verde">
								<h3>Blindagem de Veículo</h3>
							</div>
							<img src="<?php echo $config['urls']['imagens']; ?>/empresa/04.jpg" alt="Blindagem" />
						</div>
						<div class="col-md-3">
							<div class="box-verde">
								<h3>Manutenção de Veículo</h3>
							</div>
							<img src="<?php echo $config['urls']['imagens']; ?>02.png" alt="Blindagem" />
						</div>
						<div class="col-md-3">
							<div class="box-verde">
								<h3>Atendimento a CIAs de seguro</h3>
							</div>
							<img src="<?php echo $config['urls']['imagens']; ?>03.png" alt="Blindagem" />
						</div>
						<div class="col-md-3">
							<div class="box-verde">
								<h3>Compra e venda de veículos</h3>
							</div>
							<img src="<?php echo $config['urls']['imagens']; ?>04.png" alt="Blindagem" />
						</div>
					</div>						
				</div>
			</div>			
			<div class="iframe">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.9310389265465!2d-46.72242568502069!3d-23.642640584642624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce51105e9dc70d%3A0x7f6dc9b0e6598230!2sR.%20%C3%81frica%20do%20Sul%2C%2052%20-%20V%C3%A1rzea%20de%20Baixo%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004730-020!5e0!3m2!1spt-BR!2sbr!4v1581520358403!5m2!1spt-BR!2sbr"></iframe>				
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>
	<script>
		$(function(){
			$("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });
    </script>

    <script>
    	$(function(){

    		$(".item").slick();
    	});
    </script>
</body>
</html>