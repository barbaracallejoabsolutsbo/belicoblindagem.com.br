<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Alvenaria</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="servicos-full">
				<div class="container">
					<h3>HIDROJATEAMENTO E VERIFICAÇÃO DA ADERÊNCIA DO REVESTIMENTO:</h3>
					<p>O hidro-jateamento a alta pressão, é a primeira etapa da preparação superficial da alvenaria, importante não somente para a remoção de eventuais partes soltas e descascamentos da pintura anterior, mas principalmente para a remoção dos fungos e sujeiras aderidas, que influenciam diretamente na durabilidade e na eficiência da aderência da nova pintura.</p>
					<p>A aplicação de agentes removedores de fungos, tais como os produtos clorados, etc, deve ser restritas aos casos realmente necessários, uma vez que tais agentes de limpeza podem deixar resíduos ou até mesmo danificar outras superfícies da fachada (alumínio, vidros fume, revestimentos, etc), bem como plantas e jardins.</p>
					<p>A seguir é executada uma verificação da aderência das alvenarias através de espatulamento / via percussão em áreas suspeitas das fachadas, inclusive em áreas de pastilhas ou cerâmicas.</p>
					<p>No tratamento das áreas de alvenaria removidas / destacadas é importante a restauração dos ferros expostos. O simples fechamento de trinca com argamassa, sem o tratamento da ferragem exposta acarretará novamente no destacamento da alvenaria, devido à expansão causada pela ferrugem e deterioração da armadura de ferro do concreto. A ferragem exposta deverá ser lixada / escovada e tratada com anti-ferruginoso inibidor de oxidação
					(tipo Ferrox), para posteriormente ser chapiscada com argamassa adesiva (tipo Bianco) e efetuada a restauração e nivelamento da superfície da alvenaria com argamassa de cimento 3:1 conforme aplicável.</p>
					<h3>SELAGEM DAS FISSURAS SUPERFICIAIS:</h3>
					<p>A selagem das fissuras superficiais (chamadas trincas de massa, não dinâmicas) é feita pela aplicação de resina elástica (Suviflex ou Coralflex), ou mesmo aplicação de textura hidrorepelente elástica, onde aplicável.</p>
					<h3>TRATAMENTO DAS TRINCAS DINÂMICAS E DA ARMADURA:</h3>
					<p>As trincas de dimensões relevantes e de comportamento dinâmico, são abertas em “cunha” com ferramenta apropriada (abre trincas) e limpas para remover resíduos soltos. A abertura recebe uma camada de fundo preparados de paredes e é preenchida com mastique acrílico vedante de elasticidade permanente (SELATRINCA ou VEDACRIL), e em seguida é aplicada uma camada de impermeabilizantes Suvifles / Coralflex e, caso necessário um reforce de tela de Poliéster e nova camada de Suviflex / Coralflex.</p>
					<p>A superfície assim tratada é recoberta com massa acrílica / textura / massa base (conforme aplicável) para uniformização da área. Nas trincas maiores, é aplicada inicialmente argamassa de cimento como base de nivelamento superficial.</p>
					<p>Nota Importante: Apesar de não existir garantia absoluta quanto ao reaparecimento de trincas, sobretudo aquelas de origem estrutural ou mesmo decorrentes das contrações do reboco, o procedimento acima descrito, implementado com seqüência e tempos corretos, permite afirmar com relativa segurança que as trincas assim tratadas não serão novamente aparentes.</p>
					<h3>ALVENARIAS:</h3>
					<p>Áreas Externas – Lavagem com hidro-jateamento a alta pressão, selagem das fissuras superficiais, abertura das trincas dinâmicas, desoxidação e tratamento das eventuais ferragem da armação expostas, preenchimento com mastiques acrílico, restauração dos substratos da alvenaria, aplicação dos fundos preparadores e seladoras, aplicação da pintura acrílica, limpeza das janelas e vidros.</p>
					<p>Áreas Internas – Tetos são corrigidos com massa corrida e pintados com duas demãos de Látex PVA conforme o padrão existente.</p>
					<p>Paredes são corrigidas e pintadas com duas demãos de Látex Acrílico no padrão existente ou a ser escolhido.</p>
					<h3>PASTILHAS OU CERÂMICAS:</h3>
					<p>Lavagem com hidro-jateamento a alta pressão, abertura das trincas dinâmicas, desoxidação e tratamento das eventuais ferragens da armação expostas, preenchimento das trincas e recolocação das pastilhas faltantes.</p>
					<h3>SUPERFÍCIES EM MADEIRAS:</h3>
					<p>Tetos de sacadas, vitrôs, venezianas e outras superfícies em madeira, são lixadas, regularizadas / niveladas com massa onde necessário e pintadas com tinta esmalte sintético ou verniz conforme aplicável.</p>
					<h3>SUPERFÍCIES EM FERRO:</h3>
					<p>Grades, portões, balaústres, vitrôs, esquadrias e outras superfícies em ferro, são lixadas, tratadas com tinta anti-ferruginosa e/ou inibidores de ferrugem onde necessário e pintadas com tinta esmalte sintético.</p>
					<h3>CONCRETO APARENTE:</h3>
					<p>Lavagem com hidro-jateamento a alta pressão, abertura das trincas dinâmicas e áreas destacadas, desoxidação e tratamento das eventuais ferragens da armação expostas, restauração do substrato de alvenaria pelo preenchimento das áreas afetadas, com argamassa de cimento aditivado com resina acrílica, estucagem para a regularização das superfícies, lixamento e aplicação das resinas e/ou vernizes adequados, limpeza das janelas.</p>
					<h3>TIJOLOS APARENTES:</h3>
					<p>Lavagem com hidro-jateamento a alta pressão, lixamento e aplicação de silicone, hidrorepelente, resinas e/ou vernizes adequados, limpeza das janelas e vidros.</p>
					<h3>MANUTENÇÃO E RESTAURAÇÃO DE FACHADAS:</h3>
					<p>A Manutenção das fachadas deve ser programada e ter periodicidade, de acordo com cada tipo acabamento e revestimento. Com isso a edificação estará desempenhando sua função de proteger as estruturas internas, plenamente apresentáveis, garantirá a segurança dos usuários, evitará uma serie de transtornos e até danos irreparáveis.</p>
					<p>A periodicidade da manutenção nunca deve ser superior a 5 anos.</p>
					<p>A BRASLIMP realiza com muita qualidade a manutenção de suas fachadas através dos seguintes serviços de restauração:</p>
					<ul>
						<li>Pintura das fachadas;</li>
						<li>Restaurtação e Rejunte das Pastilhas;</li>
						<li>Troca de revestimentos;</li>
						<li>Aplicação de texturas;</li>
						<li>Recuperação estrutural;</li>
						<li>Impermeabilização das Fachadas;</li>
						<li>Vedação de Janelas e Esquadrias. (Pele de Vidro);</li>
						<li>Pintura ou Textura sob Pastilhas;</li>
						<li>Tratamento do Concreto aparente e Tijolinho a vista;</li>
						<li>Impermeabilização de lajes;</li>
						<li>Aplicação de hidro-repelente;</li>
						<li>Fulget e Granilha;</li>
						<li>Tratamento de Manchas em alumínios e vidros;</li>
						<li>Tratamento de fissuras e Trincas Dinamicas;</li>
						<li>Fixação de Placas de Granito, Mármores e Alucobond ACM;</li>
						<li>Tratamento de Ferragens Expostas;</li>
						<li>Lixamento, Polimento e Estucagem de Concreto Aparente, Mármores e Granitos</li>
					</ul>
					<h3>RETROFIT</h3>
					<p>Revitalizar antigos edifícios, aumentando sua vida útil, através da incorporação de modernas tecnologias e utilização dos mais avançados materiais.</p>
					<p>O retrofit tem o sentido de renovação, onde se pressupõem uma intervenção integral, obrigando-se ao encontro de soluções completas nas fachadas que caracterizam seu posicionamento no que exista de melhor no mercado.</p>
					<p>Neste sentido percebe-se que difere substancialmente da simples restauração, que consiste na restituição do imóvel à sua condição original, ou da reforma, que visa à introdução de melhorias, sem compromisso com suas características anteriores.</p>
					<ul>
						<li>Valorização de pelo menos 30% no valor do imóvel;</li>
						<li>Facilidade de comercialização e locação;</li>
						<li>Redução nas despesas de manutenção;</li>
						<li>Evita problemas com acidentes no desprendimento de placas em cima dos usuários;</li>
						<li>Atualização arquitetônica e estética.</li>
					</ul>
					<h3>REMOÇÃO DE MANCHAS DE VIDROS / IRISAÇÃO:</h3>
					<p>Machas provocadas por intempéries, exposição ao sol, chuvas acidas, produtos químicos, etc.</p>
					<p>Todo trabalho consiste na remoção total das manchas, com produto especifico e Mao de obra especializada para a execução do trabalho</p>
					<h3>OBRAS ESPECIAIS:</h3>
					<p>Readequação do uso de espaços</p>
					<p>Edificações construídas há 20 ou 30 anos não foram concebidas dentro de uma realidade de segurança, comodidade e lazer diferente da atual.</p>
					<p>A BRASLIMP, diante de uma demanda crescente pela readequação de uso dos espaços em construções mais antigas, oferece aos seus clientes projetos de Retrofit, alinhando a arquitetura existente com as mais novas tendências de mercado e necessidades contemporâneas de seus usuários.</p>
					<p>Para execução de tais obras é sempre levada em consideração a valorização dos imóveis, que em certas obras chegaram a dobra de valor.</p>
					<p>Projetos focados no aumento de segurança Patrimonial:</p>
					​<p>Readequação de acesso ao prédio incluindo construções de guaritas, marquises, cancelas, sala blindadas de monitoramento, construção de escadas contra incêndio externas ao prédio, entre outros.</p>
					<p>Projetos focados no aumento da área de Lazer:​</p>
					<p>Construções de guaritas brindadas, espaços gourmet, quadras esportivas, salas de ginasticas entre outros serviços.</p>
					<p>Projetos focados no aumento da comodidade dos usuários:</p>
					<p>Construção de rampas de acessibilidade;​</p>
					<p>Aumento do número de vagas de garagem, acessibilidade a portadores de necessidades físicas, entre outros.					</p>
				</div>	
			</div>				
		</main>

		<?php require PARTE.'footer.php'; ?>

	</body>
	</html>