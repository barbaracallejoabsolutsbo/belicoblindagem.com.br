<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="banner-empresa">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Orçamento</h2>
					</div>

				</div>
			</div>
		</div>		
		<div class="container">
			<div class="conteudo">
				<div class="texto">
					<div class="row">
						<div class="col-md-4">
							<div class="hentry">
								<div class="widget_black-studio-tinymce">
									<div class="featured-widget">
										<h3 class="widget-title">
											<span class="widget-title__inline">LOCALIZAÇÃO</span>
										</h3>
										<p><strong>Bélico Blindagem</strong><br><br>
											<i class="fa fa-map-pin"></i> R. África do Sul, 52 - Santo Amaro   <br>São Paulo - CEP: 04730-020</p>
											<p>(11) 3088-8383</p>
											<p><i class="fa fa-whatsapp"></i> 11 98742-3131 - Atendimento</p>
											<p><i class="fa fa-envelope"></i> belicoblindagem@belicoblindagem.com.br</p>
										</div>
									</div>
								</div>
							</div>						
							<div class="col-md-8">								
								<div class="contato-formulario">
									<h3>Preencha os dados para enviarmos um orçamento</h3>
									<?php require PARTE.'formulario-orca.php'; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="iframe-ctt">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.9310389265465!2d-46.72242568502069!3d-23.642640584642624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce51105e9dc70d%3A0x7f6dc9b0e6598230!2sR.%20%C3%81frica%20do%20Sul%2C%2052%20-%20V%C3%A1rzea%20de%20Baixo%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004730-020!5e0!3m2!1spt-BR!2sbr!4v1581520358403!5m2!1spt-BR!2sbr"></iframe>				
			</div>			
		</main>

		<?php require PARTE.'footer.php'; ?>

	</body>
	</html>