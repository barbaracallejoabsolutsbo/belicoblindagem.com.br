<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="base">
			<div class="base-content">

				<div class="conteudo">

					<div class="texto">
						<h1><?php echo TITULO; ?></h1>

						<div class="erro404">404 - Página não encontrada</div>

					</div>

					<?php require PARTE.'sidebar.php'; ?>

				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>