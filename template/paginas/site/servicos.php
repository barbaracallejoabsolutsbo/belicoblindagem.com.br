<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Serviços</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="quems-somos-ful">
				<div class="container">
					<div class="row">
						<div class="col-md-4">							
							<ul>
								<li>Manutenção de vidros blindados;</li>
								<li>Redimensionamento de suspensão;</li>
								<li>Pinturas e Cristalização;</li>
								<li>Higienização;</li>
								<li>Revisão mecânica e elétrica;</li>
							</ul>
							<h3>Atendimento às Cias de Seguros:</h3>
							<p>Atendemos a todas Cias de Seguros, em todo território nacional, com atendimento já realizado em 18 Estados Brasileiros.</p>
							<img src="<?php echo $config['urls']['imagens']; ?>cias.jpg" alt="Empresa Full" class="img-responsive"/>

						</div>
						<div class="col-md-8">
							<img src="<?php echo $config['urls']['imagens']; ?>servicos.jpg" alt="Empresa Full" class="img-responsive"/>

						</div>
					</div>
				</div>	
			</div>	
		</div>			
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>