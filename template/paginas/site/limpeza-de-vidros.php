<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Limpeza</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="servicos-full">
				<div class="container">
					<h3>LAVAGEM DE FACHADAS</h3>
					<p>São dois tipos de tratamento de higienização das fachadas:</p>
					<p>A lavagem das fachadas se dá pelo hidro-jateamento utilizando os produtos adequados a cada tipo de acabamento, como Texturas, Tinta acrílica, Pastilhas, Cerâmicas, Fulget, Mármores, Placas de ACM entre outras…</p>
					<p>Muitas vezes apenas a lavagem é capaz de renovar as fachadas, desta forma evitando a necessidade de pintura e assim realizar uma significativa economia aos condôminos.</p>
					<h3>LIMPEZA DE VIDROS</h3>
					<p>A limpeza de vidros deve ser realizada com o uso de esfregões e rodos, garantindo que todas as partículas de sujeira e poluição sejam retiradas dos vidros e assim possibilitar uma transparência plena.
					Como a necessidade de limpeza dos vidros é mais frequente, temos a opção de contrato de manutenção programada, ou seja, com pequenos pagamentos mensais existe uma programação de limpeza para os 12 meses.</p>
					<p>Limpeza de vidros com profissionais qualificados</p>
					<p>Nossa empresa oferece a limpeza de vidros nos mais variados ambientes, a pedido do cliente. Com profissionais altamente qualificados, obedecendo todas as normas de segurança e de trabalho, e pensando em garantir o aumento da produtividade e melhoria da qualidade dos serviços, a limpeza de vidros realizada pela Limter possui equipamentos e produtos com garantia comprovada.</p>
					<p>Nossa equipe para a limpeza de vidros passa por treinamento específico para realizar o trabalho, além de uma avaliação constantes das condições físicas e psicológicas, frisando o bem-estar e satisfação do cliente após contratar a limpeza de vidros.</p>
					<p>Como é feita a limpeza de vidros?</p>
					<p>A limpeza de vidros realizada pela BRASLIMP é garantia de qualidade por muito mais tempo. Ao contrário do que a maioria pensa, a limpeza de vidros precisa ser realizada de uma maneira séria e eficaz, já que procedimentos incorretos podem comprometer o revestimento do vidro.</p>
					<h3>REMOÇÃO DE MANCHAS DE VIDROS / IRISAÇÃO:</h3>
					<p>Machas provocadas por intempéries, exposição ao sol, chuvas acidas, produtos químicos, etc.
					</p>
					<p>Todo trabalho consiste na remoção total das manchas, com produto especifico e Mao de obra especializada para a execução do trabalho.</p>
				</div>	
			</div>	
		</div>			
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>