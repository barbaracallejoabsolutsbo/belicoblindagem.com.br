<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="banner-empresa">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Nossas Obras</h2>
						</div>
						<div class="col-md-6 text-right">
							<?php require PARTE.'breadcrumb.php'; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="obras-full">
				<div class="container">
					<div class="grid">						
						<div class="row">							
							<div class="col-md-4">
								<figure class="effect-oscar">
									<img src="<?php echo URL; ?>template/imagens/obras/sem-agua.jpg" alt="" class="img-right">
									<figcaption>
										<h2><span>Limpeza</span></h2>
										<p>Sem Utilização de água</p>
										<a href="<?php echo URL; ?>limpeza-sem-utilizacao-de-agua">View more</a>
									</figcaption>			
								</figure>
							</div>
							<div class="col-md-4">
								<figure class="effect-oscar">
									<img src="<?php echo URL; ?>template/imagens/obras/hsbc.jpg" alt="" class="img-right">
									<figcaption>
										<h2><span>HSBC</span></h2>
										<a href="<?php echo URL; ?>hsbc">View more</a>
									</figcaption>			
								</figure>
							</div>
							<div class="col-md-4">
								<figure class="effect-oscar">
									<img src="<?php echo URL; ?>template/imagens/obras/graduada.jpg" alt="" class="img-right">
									<figcaption>
										<h2><span>Graduada</span></h2>
										<p>Impermeabilização de Lajes</p>
										<a href="<?php echo URL; ?>graduada-impermeabilizacao-de-lajes">View more</a>
									</figcaption>			
								</figure>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<figure class="effect-oscar">
									<img src="<?php echo URL; ?>template/imagens/obras/paulista.jpg" alt="" class="img-right">
									<figcaption>
										<h2>Ed. <span>Paulista</span></h2>
										<p>1100</p>
										<a href="<?php echo URL; ?>ed-paulista-1100">View more</a>
									</figcaption>			
								</figure>
							</div>
							<div class="col-md-4">
								<figure class="effect-oscar">
									<img src="<?php echo URL; ?>template/imagens/obras/image.jpg" alt="" class="img-right">
									<figcaption>
										<h2>Ed. <span>Image</span></h2>
										<p>Tratamento de Trincas</p>
										<a href="<?php echo URL; ?>ed-image-tratamento-de-trincas">View more</a>
									</figcaption>			
								</figure>
							</div>
							<div class="col-md-4">							
								<div class="img-pc">
									<figure class="effect-oscar">
										<img src="<?php echo URL; ?>template/imagens/obras/berrini.jpg" alt="" class="img-right">
										<figcaption>
											<h2>Ed. <span>Berrini</span></h2>
											<p>500</p>
											<a href="<?php echo URL; ?>ed-berrini-500">View more</a>
										</figcaption>			
									</figure>
								</div>
							</div>
						</div>
					</div>	
				</div>	
			</div>			
		</main>

		<?php require PARTE.'footer.php'; ?>

	</body>
	</html>