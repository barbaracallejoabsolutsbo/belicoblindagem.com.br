<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="main-content">
			<div class="theme-default-nivo-slider">
				<div id="slider" class="nivoSlider"> 
					<img src="<?php echo $config['urls']['imagens']; ?>slide-empresa.jpg" alt="Banner" />
					<img src="<?php echo $config['urls']['imagens']; ?>slide-empresa-2.jpg" alt="Banner" />
					<img src="<?php echo $config['urls']['imagens']; ?>slide-empresa-3.jpg" alt="Banner" />
					<img src="<?php echo $config['urls']['imagens']; ?>slide-empresa-4.jpg" alt="Banner" />
				</div>
			</div>
			<div class="obras-empresa">
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							<h3>Quem Somos</h3>
							<p>A BÉLICO BLINDAGEM foi criada para proporcionar a você cliente especial a oportunidade de proteger seu bem mais precioso, sua família! Utilizamos materiais conceituados no mercado, mão de obra altamente qualificada, com atenção minuciosa aos detalhes de acabamento, apresentando assim, um serviço sob medida para cada cliente. Pois aqui você é ÚNICO!</p>
						</div>
						<div class="col-md-6">
							<h3>DIFERENCIAIS</h3>
							<ul>
								<li>• Pós-venda: Atendimento imediato, sem agendamento, atendemos em domicílio;</li>
								<li>• Acabamento impecável;</li>
								<li>• Funcionários qualificados com mais de 15 anos de experiência;</li>
								<li>• Entrega dentro do prazo contratado;</li>
								<li>• Utilização de materiais regulamentados;</li>
								<li>• Certificado da Polícia Civil e Exército Brasileiro.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="obras-empresa-1">
				<h3>Certificados:</h3>
				<img src="<?php echo $config['urls']['imagens']; ?>certificados.jpg" alt="Banner" />
			</div>	

			<div class="galeria-empresa" style="float: left; width: 100%; background: #fff;">
				<div class="container">
					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Sala de reunião</h2>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/reuniao-1.jpg">
						</div>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/reuniao-2.jpg">
						</div>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/reuniao-3.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Sala de espera</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/espera-1.jpg">
						</div>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/espera-2.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Producão</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/producao-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Blindagem Belico</h2>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/blindagem-belico-1.jpg">
						</div>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/blindagem-belico-2.jpg">
						</div>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/blindagem-belico-3.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Fazemos blindagem com os vidros dianteiro com  100 % de abertura</h2>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/dianteiro-1.jpg">
						</div>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/dianteiro-2.jpg">
						</div>
						<div class="col-md-4">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/dianteiro-3.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Fazemos Manutencao e revisão de blindagem</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/manutencao-1.jpg">
						</div>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/manutencao-2.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Blindagem interna com manta e aço</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/interna-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Blindagem de coluna em aço</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/coluna-1.jpg">
						</div>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/coluna-2.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Proteção com plástico para iniciar a blindagem</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/plastico-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Blindagem traseira do veículo</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/traseira-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Blindagem de porta</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/porta-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Fazemos a cinta de roda</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/roda-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Entregamos seu carro limpo</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/carro-limpo-1.jpg">
						</div>
					</div>

					<div class="galeria" style="float: left; width: 100%; margin-bottom: 50px;">
						<h2>Sala de espera com exposição de vidro blindado, cinta de roda , teste de bala em aço e manta</h2>
						<div class="col-md-6">
							<img class="img-responsive" src="<?php echo $config['urls']['imagens']; ?>empresa/exposicao-1.jpg">
						</div>
					</div>


				</div>
			</div>

		</div>		
	</main>

	<?php require PARTE.'footer.php'; ?>
	<script>
		$(function(){
			$("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });
    </script>

    <script>
    	$(function(){

    		$(".item").slick();
    	});
    </script>
</body>
</html>