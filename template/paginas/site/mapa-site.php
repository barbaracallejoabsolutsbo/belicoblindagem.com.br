<?php require PARTE.'head.php'; ?>
</head>
<body>

	<?php require PARTE.'topo.php'; ?>

	<main>
		<div class="banner-empresa">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2><?php echo TITULO; ?></h2>
					</div>
					<div class="col-md-6 text-right">
						<?php require PARTE.'breadcrumb.php'; ?>
					</div>
				</div>
			</div>
		</div>			
		<div class="base">
			<div class="base-content">
				<div class="conteudo">
					<div class="texto">

						<h3>Saiba mais sobre nossos serviços</h3>
						<ul class="mapa-do-site">
							<?php
							foreach ($seo['paginasSeo'] as $key => $value) {
								echo '<li><a href="'.URL.ltrim($key, '/').'">'.$value['title'].'</a></li>';
							}
							?>
						</ul>

						<h3>Outras páginas</h3>
						<ul class="mapa-do-site">
							<?php
							foreach ($seo['paginasSite'] as $key => $value) {
								echo '<li><a href="'.URL.ltrim($key, '/').'">'.$value['title'].'</a></li>';
							}
							?>
						</ul>

						<a href="<?php echo URL; ?>">Voltar para a home</a>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php require PARTE.'footer.php'; ?>

</body>
</html>