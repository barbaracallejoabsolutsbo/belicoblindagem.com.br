<?php

    $title       = "Slick";
    $description = "";    
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/quality/class.quality.php"; 
    include "includes/_parametros.php";
    include "includes/quality/head.quality.php";
    
    $quality->compressCSS(array(
        "tools/slick"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        
        <div class="container">
            
            <p>Documentação <a href="http://kenwheeler.github.io/slick/" target="_blank">aqui</a></p>
        
            <div class="item">
                <div><h3>1</h3></div>
                <div><h3>2</h3></div>
                <div><h3>3</h3></div>
                <div><h3>4</h3></div>
                <div><h3>5</h3></div>
                <div><h3>6</h3></div>
            </div>

            <hr>

            <div class="multiple-items">
                <div><h3>1</h3></div>
                <div><h3>2</h3></div>
                <div><h3>3</h3></div>
                <div><h3>4</h3></div>
                <div><h3>5</h3></div>
                <div><h3>6</h3></div>
            </div>

            <hr>

            <div class="autoplay">
                <div><h3>1</h3></div>
                <div><h3>2</h3></div>
                <div><h3>3</h3></div>
                <div><h3>4</h3></div>
                <div><h3>5</h3></div>
                <div><h3>6</h3></div>
            </div>

        </div>
        
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $quality->compressJS(array(
        "tools/jquery.slick"
    )); ?>
    
    <script>
        $(function(){

            $(".item").slick();

            $(".multiple-items").slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });

            $(".autoplay").slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000
            });

        });
    </script>
    
</body>
</html>