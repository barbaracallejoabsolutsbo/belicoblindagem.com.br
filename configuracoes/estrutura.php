<?php
if(isset($seo['paginasSeo'][PAGINA])) {
	$loadTemplate = $config['pastas']['seo'];
} else if(isset($seo['paginasSite'][PAGINA]) || PAGINA === '/') {
	$loadTemplate = $config['pastas']['site'];
} else {
	$loadTemplate = false;
}

if($loadTemplate !== false) {
	$paginaBarraInvertida = str_replace('/', DS, PAGINA);
	$paginaBarraInvertida = ltrim(rtrim($paginaBarraInvertida, DS), DS);

	$paginaPath = $loadTemplate.$paginaBarraInvertida;

	$final = explode(DS, $paginaPath);
	$final = end($final);

	if($paginaBarraInvertida === '') {
		if(file_exists($paginaPath.'index.php')) {
			define('ARQUIVO', $paginaPath.'index.php');
		} else {
			exit('O arquivo index.php não foi encontrado.');
		}
	} else {
		if(file_exists($paginaPath.'.php')) {
			define('ARQUIVO', $paginaPath.'.php');
		} else if(file_exists($paginaPath.$final.DS.$final.'.php')) {
			define('ARQUIVO', $paginaPath.$final.DS.$final.'.php');
		}
	}
}

if(!defined('ARQUIVO')) {
	define('ARQUIVO', $config['pastas']['site'].'404.php');
}
