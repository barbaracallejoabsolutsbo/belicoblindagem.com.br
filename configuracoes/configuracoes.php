<?php
$config = array();

$pastaProjeto = '/'; // Barra no começo e no final
$protocolo = 'http';

$config['site'] = array(
	'nomeSite' => 'Bélico Blindagem',
	'slogan' => 'Slogan da Empresa',
	'telefone' => array(
		'(11) 3088-8383',
	),
	'emailContato' => array(
		'carlos@belicoblindagem.com.br',
	),
	'endereco' => array(
		'rua' => 'R. África do Sul, 52',
		'bairro' => 'Santo Amaro',
		'cidade' => 'São Paulo',
		'uf' => 'SP',
		'cep' => '04730-020',
		'latitude' => '-23.642415',
		'longitude' => '-46.720264',
		'placename' => '',
		'region' => '',
	),
);

$config['smtp'] = array(
	'Username' => 'admin@mybossdobrasil.com.br',
	'Host' => 'h57.servidorhh.com',
	'Password' => 'g)9Lc&2%HTH4',
	'SMTPSecure' => 'tls',
	'Port' => 587,
	'isSMTP' => true,
	'SMTPAuth' => true,
	'SMTPDebug' => false,
	'Timeout' => 10
);

$config['formulario'] = array(
	'nomeRemetente' => 'Formulário do Site', // Caso não tenha nome do cliente
	'emailRemetente' => 'admin@mybossdobrasil.com.br', // Caso não tenha email do cliente
	'emails' => array(
		'receberEnvios' => array(
			'belicoblindagem@belicoblindagem.com.br',
		),
		'receberEnviosCopia' => array(
			'vitor.previato.absolutsbo@gmail.com'
		),
		'receberEnviosCopiaOculta' => array(),
	),
);

$config['creditos'] = array(
	'topo' => '
	Desenvolvido por Absolut SBO
	Site: www.absolutsbo.com.br
	Contato: relacionameno@absolutsbo.com.br
	',
	'nome' => 'Absolut SBO',
	'link' => 'http://www.absolutsbo.com.br',
);


$config['outros'] = array(
	'cssInline' => true,
	'jsInline' => true,
);

/*
 * Configurações de URL
 * -------------------------------------- */

$url = $protocolo.'://'.$_SERVER['SERVER_NAME'];
$url = rtrim($url, '/');
$url = $url.$pastaProjeto;
define('URL', $url);

/*
 * Configurações necessárias, não remover
 * -------------------------------------- */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)).DS);

$config['urls'] = array(
	'template' => $url.'template/',
	'css' => $url.'template/css/',
	'js' => $url.'template/js/',
	'imagens' => $url.'template/imagens/',
);

$config['pastas'] = array(
	'configuracoes' => ROOT.'configuracoes'.DS,
	'template' => ROOT.'template'.DS,
	'css' => ROOT.'template'.DS.'css'.DS,
	'imagens' => ROOT.'template'.DS.'imagens'.DS,
	'js' => ROOT.'template'.DS.'js'.DS,
	'paginas' => ROOT.'template'.DS.'paginas'.DS,
	'partes' => ROOT.'template'.DS.'partes'.DS,
	'seo' => ROOT.'template'.DS.'paginas'.DS.'seo'.DS,
	'site' => ROOT.'template'.DS.'paginas'.DS.'site'.DS,
);

$pagina = '/'.str_replace($pastaProjeto, '', $_SERVER['REQUEST_URI']);

if(substr($pagina, -1) === '/') {
	$pagina = rtrim($pagina, '/');
}

$pagina = strtok($pagina, '?');
$pagina = strtok($pagina, '#');

if(strlen($pagina) === 0) {
	$pagina .= '/';
}

define('PAGINA', $pagina);
define('PARTE', $config['pastas']['partes']);
