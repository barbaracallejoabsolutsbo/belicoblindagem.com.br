<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function cssInline($arquivo) {
	global $config;
	$ret = '';
	$cont = 0;
	$atual = '';
	foreach ($arquivo as $key => $value) {
		if(!$config['outros']['cssInline']) {
			$keyBarra = str_replace('/', DS, $key);
			if(file_exists($config['pastas']['css'].$keyBarra)) {
				$ret .= '<link rel="stylesheet" type="text/css" href="'.$config['urls']['css'].$key.'" />'."\n";
			}
		} else {
			$atual = '';
			$cont == 0 ? $ret .= '<style>' : '';
			$file = str_replace('/', DS, $key);
			$file = $config['pastas']['css'].$file;

			if(file_exists($file)) {
				$atual = file_get_contents($file);

				$atual = str_replace(array("../../../../../", "../../../../", "../../../", "../../", "../", "./"), $config['urls']['template'], $atual);

				if($value) {
					$atual = trim($atual);
					$atual = str_replace(array("\n", "\r", "\t"), "", $atual);
					$atual = str_replace(": ", ":", $atual);
					$atual = str_replace(", ", ",", $atual);
					$atual = str_replace("; ", ";", $atual);
					$atual = str_replace(";   ", ";", $atual);
					$atual = str_replace("{ ", "{", $atual);
					$atual = str_replace(" {", "{", $atual);
					$atual = str_replace(" { ", "{", $atual);
					$atual = str_replace("   {", "{", $atual);
					$atual = str_replace("} ", "}", $atual);
					$atual = str_replace(" }", "}", $atual);
					$atual = str_replace(" } ", "}", $atual);
					$expComentario = '!/\*[^*]*\*+([^/][^*]*\*+)*/!';
					$atual = preg_replace($expComentario, '', $atual);
				}
			}

			$ret .= $atual;
			$cont++;
			$cont == count($arquivo) ? $ret .= '</style>' : '';
		}
	}

	return $ret;
}

/*
 * Adiciona os arquivos JS inline
 */
function jsInline($arquivo) {

	global $config;

	if(!is_array($arquivo)) {
		exit('O arquivo jsInline precisa estar no formato array.');
	}

	$ret = '';
	$cont = 0;
	$atual = '';
	foreach ($arquivo as $key => $value) {
		if(!$config['outros']['jsInline']) {
			$keyBarra = str_replace('/', DS, $key);
			if(file_exists($config['pastas']['css'].$keyBarra)) {
				$ret .= ($cont === 0 ? "\n" : '').'<script src="'.$config['urls']['js'].$key.'"></script>'."\n";
			}
			$cont++;
		} else {
			$atual = '';
			$cont == 0 ? $ret .= '<script>' : '';
			$file = str_replace('/', DS, $key);
			$file = $config['pastas']['js'].$file;

			if(file_exists($file)) {
				$atual = file_get_contents($file);
				$expressaoJS = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
				if($value) {
					$atual = preg_replace($expressaoJS, '', $atual);
					$atual = str_replace(array("\n","\t","\r"), '', $atual);
				}
			}
			$ret .= $atual;
			$cont++;
			$cont == count($arquivo) ? $ret .= '</script>' : '';
		}
	}

	return $ret;
}

function validarCampos($camposEnviados, $camposValidar) {

	$campos = array();
	if(is_array($camposEnviados)) {
		foreach ($camposEnviados as $key => $value) {
			if(in_array($key, $camposValidar)) {
				if($value == '') {
					$campos[$key] = $key;
				}
			}
		}
	}

	if(!empty($campos)) {
		$msg = '';
		foreach ($campos as $key => $value) {
			$msg .= '<strong>'.$key.'</strong>, ';
		}
		$msg = rtrim($msg, ', ');
		exit(json_encode(array(
			'success' => false,
			'inputs' => $campos,
			'msg' => '<div>Preencha todos os campos obrigatórios:</div><div>'.$msg.'</div>'
		)));
	}

}

function imgDestaque($numero = false, $thumb = false) {
	global $config;
	$ret = '';

	$ret .= '
		<div>
			<a href="'.$config['urls']['imagens'].'palavras-chave/'.trim(PAGINA, '/').($numero ? '-'.$numero : '').'.jpg" data-lightbox>
				<img src="'.$config['urls']['imagens'].'palavras-chave/'.($thumb ? 'thumb/' : '').trim(PAGINA, '/').($numero ? '-'.$numero : '').'.jpg" alt="'.TITULO.'">
			</a>
		</div>
	';

	return $ret;
}

function enviarEmail($post,	$nomeRemetente,	$emailRemetente, $titulo, $files = false) {

	global $config;
	$mail = new PHPMailer(true);

	$config['smtp']['isSMTP'] ? $mail->isSMTP() : '';
	$mail->isHTML(true);
	$mail->CharSet = 'UTF-8';
	$mail->Host = $config['smtp']['Host'];
	$mail->SMTPAuth = $config['smtp']['SMTPAuth'];
	$mail->Username = $config['smtp']['Username'];
	$mail->Password = $config['smtp']['Password'];
	$mail->SMTPSecure = $config['smtp']['SMTPSecure'];
	$mail->Port = $config['smtp']['Port'];
	$mail->SMTPDebug = $config['smtp']['SMTPDebug'];
	$mail->Timeout = $config['smtp']['Timeout'];

	$mail->setFrom($emailRemetente, $nomeRemetente);
	$mail->addReplyTo($emailRemetente, $nomeRemetente);

	// Email de contato
	if(is_array($config['formulario']['emails']['receberEnvios']) && !empty($config['formulario']['emails']['receberEnvios'])) {
		foreach ($config['formulario']['emails']['receberEnvios'] as $keyTo => $valueTo) {
			if(!empty($valueTo)) {
				$mail->addAddress($valueTo);
			}
		}
	}

	// Email de contato (cópia)
	if(is_array($config['formulario']['emails']['receberEnviosCopia']) && !empty($config['formulario']['emails']['receberEnviosCopia'])) {
		foreach ($config['formulario']['emails']['receberEnviosCopia'] as $keyTo => $valueTo) {
			if(!empty($valueTo)) {
				$mail->addCC($valueTo);
			}
		}
	}

	// Email de contato (cópia oculta)
	if(is_array($config['formulario']['emails']['receberEnviosCopiaOculta']) && !empty($config['formulario']['emails']['receberEnviosCopiaOculta'])) {
		foreach ($config['formulario']['emails']['receberEnviosCopiaOculta'] as $keyTo => $valueTo) {
			if(!empty($valueTo)) {
				$mail->addBCC($valueTo);
			}
		}
	}

	// Inserir anexos
	if(is_array($files) && !empty($files)) {
		foreach($files as $nomesCamposAnexos => $dadosAnexos) {
			foreach($dadosAnexos as $nomeCampoAnexo => $dadosAnexo) {
				if($nomeCampoAnexo != '') {
					$mail->addAttachment($files[$nomesCamposAnexos]['tmp_name'], $files[$nomesCamposAnexos]['name']);
				}
			}
		}
	}

	$msg = '';
	$msg .= '<div>
	<h3>'.$titulo.'</h3>';
	unset($post['ajax']);
	foreach($post as $campo => $valor) {
		$msg .= '<div><strong>'.str_replace('_', ' ', $campo).':</strong> '.str_replace(array("\n", "\t"), '<br />', strip_tags($valor));
	}

	$data = date('d/m/Y').' às '.date('H:i:s');
	$ip = $_SERVER['REMOTE_ADDR'];

	$msg .= '<div><br /><strong>Email enviado no dia:</strong> '.$data.'</div>';
	$msg .= '</div>';

	$mail->Subject = $titulo;
	$mail->Body    = $msg;

	try {
		$mail->send();
		return array(
			'success' => true,
			'msg' => 'Mensagem enviada com sucesso!'
		);
	} catch (Exception $e) {
		return array(
			'success' => false,
			'msg' => 'Não foi possível enviar a mensagen: '.$e->getMessage()
		);
	}

}
