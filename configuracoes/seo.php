<?php
$seo = array();

/*
 * Outras páginas
 * -------------------------------------- */
$seo['paginasSite'] = array(
	'/' => array(
		'title' => 'Blindagem de Automóveis',
		'description' => 'Bem vindo a Braslimp, somos uma empresa especializada na limpeza e manutenção de fachadas prediais.',
		'keywords' => 'Blindagem de Automóveis',
	),
	'/informacoes' => array(
		'title' => 'Informações',
		'description' => 'Em nosso página de informações você encontra diversos produtos e serviços, confira nossas categorias e informações de nossos produtos e serviços.',
		'keywords' => 'Informações',
	),
	'/mapa-site' => array(
		'title' => 'Mapa do Site',
		'description' => 'Em nosso mapa do site você encontra todas as páginas do site, confira nossas categorias e informações de nossos produtos e serviços.',
		'keywords' => 'Mapa do Site',
	),
	'/contato' => array(
		'title' => 'Contato',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Contato',
	),
	'/orcamento' => array(
		'title' => 'Orçamento',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Orçamento',
	),	
	'/empresa' => array(
		'title' => 'Empresa',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Empresa',
	),
	'/blindagem' => array(
		'title' => 'Blindagem',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Blindagem',
	),	
	'/servicos' => array(
		'title' => 'Serviços',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Serviços',
	),
	'/parcerias' => array(
		'title' => 'Parcerias',
		'description' => 'Entre em contato conosco e envie sua mensagem ou solicitação de orçamento, nossa equipe entrará em contato assim que possível para atendê-lo.',
		'keywords' => 'Parcerias',
	),
);

/*
 * Páginas de SEO
 * -------------------------------------- */
$seo['paginasSeo'] = array(
	'/blindagem-de-veiculos' => array(
		'title' => 'Blindagem de Veículos',
		'description' => 'A Bélico Blindagem é uma empresa de blindagem de veículos de Santo Amaro, atuante em São Paulo e regiões adjacentes, porem aceitamos projetos de todo o território nacional.',
		'keywords' => 'Blindagem de Veículos',
	),	
	'/blindadora-em-sao-paulo' => array(
		'title' => 'Blindadora em São Paulo',
		'description' => 'Procurando pela melhor blindadora em São Paulo você acabou de encontrar a Bélico Blindagem. Somos especialistas em todo e qualquer serviço de blindagem que possa aparecer.',
		'keywords' => 'Blindadora em São Paulo',
	),
	'/blindadora-zona-sul-sao-paulo' => array(
		'title' => 'Blindadora zona Sul São Paulo',
		'description' => 'Cada revestimento aplicado pela blindadora zona sul São Paulo possui uma particularidade que ajuda a proteger os carros blindados. Cerca de 90% do carro é coberto com uma manta de fibras de aramida.',
		'keywords' => 'Blindadora zona Sul São Paulo',
	),
	'/blindagem-automotiva-em-sao-paulo' => array(
		'title' => 'Blindagem Automotiva em São Paulo',
		'description' => 'Portanto, ao procurar por blindagem automotiva em São Paulo com excelência e o melhor custo benefício, entre em contato com a Bélico Blindagem, para garantir a sua segurança.',
		'keywords' => 'Blindagem Automotiva em São Paulo',
	),
	'/blindagem-automotiva-nivel-I-a-II-a-III-a' => array(
		'title' => 'Blindagem automotiva nível I-A, II-A, III-A',
		'description' => 'Pela complexidade do processo de blindagem automotiva nível I-A, II-A, III-A, valores muito baixos podem ser suspeitos. Para ser blindado, o veículo é completamente desmontado, mantendo-se apenas a lataria',
		'keywords' => 'Blindagem automotiva nível I-A, II-A, III-A',
	),	
	'/blindagem-de-veiculos-em-santo-amaro' => array(
		'title' => 'Blindagem de Veículos em Santo Amaro',
		'description' => 'Portanto, ao procurar por blindagem de veículos em Santo Amaro com excelência e o melhor custo benefício, entre em contato com a Bélico Blindagem, para garantir a sua segurança.',
		'keywords' => 'Blindagem de Veículos em Santo Amaro',
	),
	'/blindagem-de-veiculos-em-sao-paulo' => array(
		'title' => 'Blindagem de Veículos em São Paulo',
		'description' => 'Todos nossos processos de trabalho de blindagem de veículos em São Paulo passam por um sistema de supervisão constante de qualidade e segurança, do inicio ao fim.',
		'keywords' => 'Blindagem de Veículos em São Paulo',
	),	
	'/blindagem-de-veiculos-III-a' => array(
		'title' => 'Blindagem de Veículos III-A',
		'description' => 'A blindagem de veículos III-A é resistente a disparos de fuzil, ele só pode ser produzido para pessoa física ou jurídica com autorização especial do Exército Brasileiro.',
		'keywords' => 'Blindagem de Veículos III-A',
	),
	'/blindagem-de-veiculos-sao-paulo' => array(
		'title' => 'Blindagem de Veículos São Paulo',
		'description' => 'A fim de atender as necessidades e exigências dos clientes que buscam a segurança de uma empresa de renome na área de blindagem de veículos São Paulo.',
		'keywords' => 'Blindagem de Veículos São Paulo',
	),
	'/blindagem-de-veiculos-todas-as-marcas' => array(
		'title' => 'Blindagem de Veículos Todas as Marcas',
		'description' => 'Portanto, ao procurar por blindagem de veículos todas as marcas com excelência e o melhor custo benefício, entre em contato com a Bélico Blindagem, para garantir a sua segurança e da sua família.',
		'keywords' => 'Blindagem de Veículos Todas as Marcas',
	),
	'/carros-blindados-com-garantia' => array(
		'title' => 'Carros Blindados com Garantia',
		'description' => 'Com um rigoroso sistema de qualidade e que garante a total satisfação dos clientes através do monitoramento de todas as etapas do processo de blindagem e da venda de carros blindados com garantia',
		'keywords' => 'Carros Blindados com Garantia',
	),	
	'/compra-de-veiculos-blindados' => array(
		'title' => 'Compra de Veículos Blindados',
		'description' => 'A Bélico Blindagem procura a venda e compra de veículos blindados oferecendo preço justo sempre, negociando preços de carros disponíveis conforme o nível de blindagem nele aplicado.',
		'keywords' => 'Compra de Veículos Blindados',
	),
	'/documentacao-de-veiculos-blindados' => array(
		'title' => 'Documentação de Veículos Blindados',
		'description' => 'Os documentos exigidos para elaborar a documentação de veículos blindados são: CPF, RG, comprovante de residência, certidão de antecedentes criminais dos distribuidores da Justiça Federal, Militar das Comarcas e Estadual.',
		'keywords' => 'Documentação de Veículos Blindados',
	),
	'/especialista-em-blindagem-automotiva' => array(
		'title' => 'Especialista em Blindagem Automotiva',
		'description' => 'A blindagem automotiva está se tornando uma medida de segurança que está se tornando cada vez mais comum no mercado atual, e a Bélico Blindagem é especialista em blindagem automotiva.',
		'keywords' => 'Especialista em Blindagem Automotiva',
	),
	'/laudo-de-blindagem-de-veiculos' => array(
		'title' => 'Laudo de Blindagem de Veículos',
		'description' => 'É preciso alertar que antes de decidir blindar seu carro, necessita-se da autorização de blindagem expedida pelo Exército Brasileiro (EB) e, após o processo, o laudo de blindagem de veículos.',
		'keywords' => 'Laudo de Blindagem de Veículos',
	),
	'/manutencao-de-blindagem-automotiva' => array(
		'title' => 'Manutenção de Blindagem Automotiva',
		'description' => 'Além da grande demanda de pessoas que procuram por empresas confiáveis para realizar a blindagem de seus veículos, a recorrência da manutenção de blindagem automotiva',
		'keywords' => 'Manutenção de Blindagem Automotiva',
	),
	'/manutencao-de-blindagem-de-veiculos' => array(
		'title' => 'Manutenção de Blindagem de Veículos',
		'description' => 'Líder em no estado com o serviço de manutenção de blindagem automotiva e como fábrica de blindagem, a Bélico Blindagem apresenta ampla experiência e conhecimento de mercado',
		'keywords' => 'Manutenção de Blindagem de Veículos',
	),	
	'/manutencao-de-maquinas-de-vidros-blindados' => array(
		'title' => 'Manutenção de Máquinas de Vidros Blindados',
		'description' => 'Por tanto se quiser saber mais sobre manutenção de maquinas de vidros blindados agende um horário e venha nos conhecer, nos localizamos na região de Santo Amaro/SP.',
		'keywords' => 'Manutenção de Máquinas de Vidros Blindados',
	),
	'/manutencao-de-vidros-blindados' => array(
		'title' => 'Manutenção de Vidros Blindados',
		'description' => 'A Bélico Blindagem oferece serviços de assistência técnica e manutenção de vidros blindados, para que você não corra o risco de ter a perda da vida útil de seu vidro blindado.',
		'keywords' => 'Manutenção de Vidros Blindados',
	),
	'/pneus-blindados' => array(
		'title' => 'Pneus Blindados',
		'description' => 'Para realizar o serviço, não é necessário ir até a Bélico Blindagem, a blindagem do conjunto de pneus blindados pode ser feita de forma independente em uma mecânica especializada.',
		'keywords' => 'Pneus Blindados',
	),
	'/assistencia-tecnica-em-blindagem-de-veiculos' => array(
		'title' => 'Assistência Técnica em Blindagem de Veículos',
		'description' => 'Serviço completo de atendimento, com assistência técnica em blindagem de veículos e manutenção preventiva para veículos blindados. Nossos clientes contam com oficina especializada na manutenção de blindados.',
		'keywords' => 'Assistência Técnica em Blindagem de Veículos',
	),
	'/atendimento-as-cias-de-seguros-para-veiculos-blindados' => array(
		'title' => 'Atendimento as CIAS de Seguros para Veículos Blindados',
		'description' => 'A procura por seguro de carro blindado subiu consideravelmente nos últimos 3 anos, por isso a Bélico desenvolveu também o atendimento as CIAS de seguros para veículos blindados.',
		'keywords' => 'Atendimento as CIAS de Seguros para Veículos Blindados',
	),
	'/protecao-de-blindagem-automotiva' => array(
		'title' => 'Proteção de Blindagem Automotiva',
		'description' => 'Somos os melhores quando se refere à proteção de blindagem automotiva preservando ao máximo possível, as características e funcionalidades originais dos mesmos.',
		'keywords' => 'Proteção de Blindagem Automotiva',
	),
	'/protecao-de-blindagem' => array(
		'title' => 'Proteção de Blindagem',
		'description' => 'A Bélico é uma das empresas que ganha cada vez mais espaço no setor de proteção de blindagem. Com showroom, assistências técnicas e uma fábrica equipada com materiais de última geração.',
		'keywords' => 'Proteção de Blindagem',
	),
	'/revisao-de-veiculos-blindados' => array(
		'title' => 'Revisão de Veículos Blindados',
		'description' => 'A revisão de veículos blindados é necessária de tempos em tempos, como o próprio carro. Isso é uma importante forma de garantir a total segurança do motorista e dos passageiros.',
		'keywords' => 'Revisão de Veículos Blindados',
	),	
	'/veiculos-blindados-com-garantia' => array(
		'title' => 'Veículos Blindados com Garantia',
		'description' => 'Nós da Bélico garantimos a total satisfação dos clientes através do monitoramento de todas as etapas do processo de blindagem e da venda de veículos blindados com garantia.',
		'keywords' => 'Veículos Blindados com Garantia',
	),
	'/veiculos-blindados-pronta-entrega' => array(
		'title' => 'Veículos Blindados Pronta Entrega',
		'description' => 'A Bélico originou-se de apaixonados por carro, de onde vem a idealização de um conceito inovador de se fazer blindagem de veículos e venda veículos blindados pronta entrega.',
		'keywords' => 'Veículos Blindados Pronta Entrega',
	),
	'/veiculos-blindados-seminovos-com-garantia' => array(
		'title' => 'Veículos Blindados Seminovos com Garantia',
		'description' => 'Cumpre lembrar que, todos nossos veículos blindados seminovos com garantia passam por processos de trabalho de blindagem automotiva rigorosos com um sistema de supervisão',
		'keywords' => 'Veículos Blindados Seminovos com Garantia',
	),
	'/veiculos-blindados-zero-km' => array(
		'title' => 'Veículos Blindados Zero Km',
		'description' => 'Realizamos o serviço de blindagem, manutenção de blindagem, venda de veículos blindados zero km, e qualquer outro serviço de blindagem, para todo o Brasil.',
		'keywords' => 'Veículos Blindados Zero Km',
	),
	'/veiculos-blindados' => array(
		'title' => 'Veículos Blindados',
		'description' => 'Portanto, ao procurar por veículos blindados com excelência e o melhor custo benefício, entre em contato com a Bélico Blindagem, para garantir a sua segurança e da sua família.',
		'keywords' => 'Veículos Blindados',
	),
	'/venda-de-veiculos-blindados' => array(
		'title' => 'Venda de Veículos Blindados',
		'description' => 'Para a preparação da venda de veículos blindados são utilizados os padrões de segurança mais rigorosos do mercado. O processo abrange todo o habitáculo do veículo, vidros, teto, portas, painel frontal, painel traseiro.',
		'keywords' => 'Venda de Veículos Blindados',
	),	
	'/vidros-blindados-automotivos' => array(
		'title' => 'Vidros Blindados Automotivos',
		'description' => 'Os vidros blindados automotivos são desenvolvidos como forma de aumentar a resistência e a segurança dos mesmos contra qualquer tipo de atrito, como armas de fogo e impactos causados externamente ao veículo.',
		'keywords' => 'Vidros Blindados Automotivos',
	),
	'/vidros-blindados-com-garantia-de-fabrica' => array(
		'title' => 'Vidros Blindados com Garantia de Fábrica',
		'description' => 'Suporta até disparos de Magnum 44 e pesa 27% menos. O VIDRO DE (17 mm* de espessura) é o que há de mais avançado em vidros blindados com garantia de fabrica.',
		'keywords' => 'Vidros Blindados com Garantia de Fábrica',
	),
	'/vidros-blindados-novos' => array(
		'title' => 'Vidros Blindados Novos',
		'description' => 'A Bélico blindagem é uma empresa tradicional localizada em Santo Amaro/SP, especializada na prestação de serviços de blindagem veicular, vidros blindados novos em vários níveis de proteção balística.',
		'keywords' => 'Vidros Blindados Novos',
	),
	'/vidros-de-laminados' => array(
		'title' => 'Vidros Dê Laminados',
		'description' => 'Em um carro blindado, os vidros são os itens que requerem maior cuidado, isso porque eles podem sofrer dê laminação com o passar do tempo e virarem vidros dê laminados.',
		'keywords' => 'Vidros Dê Laminados',
	),
	'/blindagem-de-veiculos-zona-sul-sao-paulo' => array(
		'title' => 'Blindagem de Veículos Zona Sul São Paulo',
		'description' => 'Cumpre lembrar que, todos nossos processos de trabalho de blindagem de veículos zona sul São Paulo passam por um sistema de supervisão constante de qualidade e segurança, do inicio ao fim.',
		'keywords' => 'Blindagem de Veículos Zona Sul São Paulo',
	),	
	'/blindagem-automotiva-zona-sul-sao-paulo' => array(
		'title' => 'Blindagem Automotiva Zona Sul São Paulo',
		'description' => 'A nossa empresa de blindagem automotiva zona sul São Paulo se diferencia no segmento de blindagem automotiva por proporcionar segurança preservando o desempenho e conforto.',
		'keywords' => 'Blindagem Automotiva Zona Sul São Paulo',
	),
	'/blindagem-de-veiculo-com-vidros-de-17-mm-a-21-mm' => array(
		'title' => 'Blindagem de Veículo com Vidros de 17mm a 21mm',
		'description' => 'A blindagem de veículo com vidros de 17 mm a 21 mm, o padrão mais utilizado pelo mercado na blindagem de carros de passeio. Trata-se de uma solução tradicional, com policarbonato de alta resistência',
		'keywords' => 'Blindagem de Veículo com Vidros de 17mm a 21mm',
	),
	'/blindagem-de-veiculos-com-acabamento-perfeito' => array(
		'title' => 'Blindagem de Veículos com Acabamento Perfeito',
		'description' => 'Por mais tecnológica que possa ser hoje em dia a blindagem, para a Bélico a blindagem de veículos com acabamento perfeito será sempre um processo artesanal, onde é exigida muita perícia.',
		'keywords' => 'Blindagem de Veículos com Acabamento Perfeito',
	),
	'/blindagem-de-veiculos-com-todas-as-exigencias-de-seguranca-do-exercito-brasileiro' => array(
		'title' => 'Blindagem de Veículos com Todas as Exigências de Segurança do Exército Brasileiro',
		'description' => 'Para solicitar a blindagem de veículos com todas as exigências de segurança do Exército brasileiro o proprietário do carro blindado precisa providenciar um cadastro na Região Militar de interesse',
		'keywords' => 'Blindagem de Veículos com Todas as Exigências de Segurança do Exército Brasileiro',
	),	
);

/*
 * Head
 * -------------------------------------- */
function seo($pagina) {

	global $seo;
	global $config;

	if(isset($seo['paginasSeo'][$pagina])) {
		$arr = $seo['paginasSeo'][$pagina];
		$imgDestaque = ltrim(rtrim($pagina, '/'), '/').'.jpg';
	} else if(isset($seo['paginasSite'][$pagina])) {
		$arr = $seo['paginasSite'][$pagina];
		$imgDestaque = ltrim(rtrim($pagina, '/'), '/').'.jpg';
	} else {
		$arr = array(
			'title' => 'Página não encontrada...',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam nisi odit, officia iste eos neque.',
			'keywords' => ''
		);
		$imgDestaque = '';
	}

	$title       = $arr['title'];
	$description = $arr['description'];
	$keywords    = $arr['keywords'];
	$urlPagina = ltrim($pagina, '/');

	if(!defined('TITULO')) {
		define('TITULO', $title);
	}

	$retorno = '<!DOCTYPE html>
	<html lang="pt-BR">
	<head>
	<meta charset="UTF-8" />
	<title>'.TITULO.' - '.$config['site']['nomeSite'].'</title>

	<!--
	'.$config['creditos']['topo'].'
	-->

	<link rel="canonical" href="'.URL.$urlPagina.'" />
	<meta name="description" content="'.$description.'" />
	<meta name="keywords" content="'.TITULO.', '.$keywords.'" />
	<meta name="googlebot" content="index, follow" />
	<meta name="robots" content="index, follow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="'.$config['urls']['imagens'].'favicon.ico" type="image/x-icon" />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:region" content="Brasil" />
	<meta property="og:title" content="'.TITULO.' - '.$config['site']['nomeSite'].'" />
	<meta property="og:author" content="'.TITULO.'" />
	<meta property="og:image" content="'.$config['urls']['imagens'].'palavras-chave/'.$imgDestaque.'" />
	<meta property="og:url" content="'.URL.$urlPagina.'" />
	<meta property="og:description" content="'.$description.'" />
	<meta property="og:site_name" content="'.$config['site']['nomeSite'].'" />
	<meta name="twitter:card" content="summary">
	<meta name="twitter:title" content="'.TITULO.'">
	<meta name="twitter:description" content="'.$description.'">
	<meta name="twitter:image" content="'.$config['urls']['imagens'].'palavras-chave/'.$imgDestaque.'">
	<meta name="geo.position" content="'.$config['site']['endereco']['latitude'].';'.$config['site']['endereco']['longitude'].'">
	<meta name="ICBM" content="'.$config['site']['endereco']['latitude'].','.$config['site']['endereco']['longitude'].'" />
	<meta name="geo.placename" content="'.$config['site']['endereco']['placename'].'">
	<meta name="geo.region" content="'.$config['site']['endereco']['region'].'">'."\n";

	return str_replace("\t", '', $retorno);
}
