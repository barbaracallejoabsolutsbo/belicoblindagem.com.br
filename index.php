<?php
require dirname(__FILE__).DIRECTORY_SEPARATOR.'init.php';
require dirname(__FILE__).DS.'configuracoes'.DS.'estrutura.php';

if(!file_exists(dirname(__FILE__).DS.'seo.php')) {
	exit('Não foi possível encontrar o arquivo seo.php');
} else {
	require dirname(__FILE__).DS.'seo.php';
	if(!defined('ARQUIVO_SEO')) {
		exit('Arquivo seo.php inválido.');
	}
}

if(file_exists(ARQUIVO)) {
	require ARQUIVO; // definido em rotas.php
} else {
	exit('O arquivo não existe.');
}
